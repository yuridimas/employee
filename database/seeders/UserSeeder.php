<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                'name' => 'Emanuel',
                'email' => 'emanuel@gmail.com',
                'password' => md5('P@ssw0rd!4dm1N!'),
                'email_verified_at' => now(),
                'is_admin' => true,
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'name' => 'Andre',
                'email' => 'andre@gmail.com',
                'password' => md5('password'),
                'email_verified_at' => now(),
                'is_admin' => false,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        DB::table('users')->insert($batch);
    }
}
