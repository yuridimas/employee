<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employee>
 */
class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sex = $this->faker->randomElement(['male', 'female']);

        return [
            'nip' => $this->faker->randomNumber($nbDigits = 8, $strict = true),
            'full_name' => $this->faker->name($sex),
            'job' => $this->faker->jobTitle(),
            'phone' => $this->faker->phoneNumber(),
            'sex' => $sex,
        ];
    }
}
