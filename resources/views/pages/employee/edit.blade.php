@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mb-4">
                            <span class="font-weight-bold">Edit Pegawai</span>
                            <span class="font-weight-bold">
                                <a href="{{ route('employee.index') }}" class="btn btn-sm btn-secondary">KEMBALI</a>
                            </span>
                        </h5>
                        <form action="{{ route('employee.update',['id' => $data->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Foto
                                    </label>
                                </div>
                                <div class="col-lg-auto">
                                    @isset($data->avatar)
                                    <img src="{{ $data->avatar_url }}" class="rounded mx-auto d-block border border-dark" width="150"
                                    height="150" alt="...">
                                    @endisset
                                    @empty($data->avatar)
                                    <img src="https://via.placeholder.com/150" class="img-thumbnail" width="150"
                                    height="150" alt="...">
                                    @endempty
                                </div>
                                <div class="col-lg">
                                    <input type="file" name="avatar" class="form-control">
                                    @error('avatar')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        NIP <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="nip" class="form-control" value="{{ old('nip', $data->nip) }}">
                                    @error('nip')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Nama Lengkap <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="full_name" class="form-control" value="{{ old('full_name', $data->full_name) }}">
                                    @error('full_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Jabatan <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="job" class="form-control" value="{{ old('job', $data->job) }}">
                                    @error('job')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Nomor Telepon <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone', $data->phone) }}">
                                    @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Jenis Kelamin <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sex" id="male" @checked($data->sex == 'male') value="male">
                                        <label class="form-check-label" for="male">Male</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sex" id="female" @checked($data->sex == 'female') value="female">
                                        <label class="form-check-label" for="female">Female</label>
                                    </div>
                                    @error('sex')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-grid gap-2 col-6 mx-auto">
                                <button type="submit" class="btn btn-success btn-block">Perbarui</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
