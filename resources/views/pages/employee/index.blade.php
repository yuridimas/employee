@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="font-weight-bold">Daftar Pegawai</span>
                            <span class="font-weight-bold">
                                <a href="{{ route('employee.create') }}" class="btn btn-sm btn-success">BUAT BARU</a>
                            </span>
                        </h5>
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Jenis Kelamin</th>
                                    <th width="15%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($employees as $item)
                                    <tr>
                                        <td>
                                            @isset($item->avatar)
                                                <img src="{{ $item->avatar_url }}" class="rounded d-block border border-dark"
                                                    width="50" height="50" alt="...">
                                            @endisset
                                        </td>
                                        <td>{{ $item->nip }}</td>
                                        <td>{{ $item->full_name }}</td>
                                        <td>{{ $item->job }}</td>
                                        <td>{{ $item->sex }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('employee.edit', ['id' => $item->id]) }}"
                                                class="btn btn-sm btn-dark">UBAH</a>
                                            @if (auth()->user()->is_admin == true)
                                                |
                                                <a href="{{ route('employee.destroy', ['id' => $item->id]) }}"
                                                    class="btn btn-sm btn-danger"
                                                    onclick="event.preventDefault();
                                                                document.getElementById('destroy-form').submit();">HAPUS</a>

                                                <form id="destroy-form"
                                                    action="{{ route('employee.destroy', ['id' => $item->id]) }}"
                                                    method="POST" class="d-none">
                                                    @csrf
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center"><span class="badge bg-warning">DATA
                                                KOSONG</span></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $employees->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
