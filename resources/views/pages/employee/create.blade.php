@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mb-4">
                            <span class="font-weight-bold">Buat Pegawai</span>
                            <span class="font-weight-bold">
                                <a href="{{ route('employee.index') }}" class="btn btn-sm btn-secondary">KEMBALI</a>
                            </span>
                        </h5>
                        <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Foto
                                    </label>
                                </div>
                                <div class="col-lg-auto">
                                    <img src="https://via.placeholder.com/150" class="rounded mx-auto d-block border border-dark" width="150"
                                        height="150" alt="...">
                                </div>
                                <div class="col-lg">
                                    <input type="file" name="avatar" class="form-control">
                                    @error('avatar')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        NIP <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="nip" class="form-control">
                                    @error('nip')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Nama Lengkap <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="full_name" class="form-control">
                                    @error('full_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Jabatan <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="job" class="form-control">
                                    @error('job')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Nomor Telepon <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="phone" class="form-control">
                                    @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-lg-2">
                                    <label class="col-form-label">
                                        Jenis Kelamin <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-lg-10">
                                    <div class="d-block">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" id="male" value="male">
                                            <label class="form-check-label" for="male">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" id="female" value="female">
                                            <label class="form-check-label" for="female">Female</label>
                                        </div>
                                    </div>
                                    @error('sex')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-grid gap-2 col-6 mx-auto">
                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
