<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::latest()->simplePaginate(5);

        return view('pages.employee.index', [
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $data = Employee::create($request->validated());

        if ($request->safe(['avatar'])) {
            $avatar = $request->file('avatar');

            $fileName = $avatar->getClientOriginalName();
            $filePath = 'back/uploads/employees/';

            $avatar->move($filePath, $fileName);
            $data->avatar = $fileName;
            $data->save();
        }

        return redirect()
            ->route('employee.index')
            ->with('success', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('pages.employee.edit', [
            'data' => $employee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $data = Employee::find($id);
        $data->update($request->validated());

        if ($request->safe(['avatar'])) {
            $avatar = $request->file('avatar');

            $fileName = $avatar->getClientOriginalName();
            $filePath = 'back/uploads/employees/';

            $upload = $avatar->move($filePath, $fileName);

            if ($upload) {
                $oldAvatar = $data->avatar;
                File::delete('back/uploads/employees/' . $oldAvatar);

                $data->avatar = $fileName;
                $data->save();
            }
        }

        return redirect()
            ->route('employee.index')
            ->with('success', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Employee::find($id);
        if ($data->avatar) {
            File::delete('back/uploads/employees/' . $data->avatar);
        }
        $data->delete();

        return redirect()
            ->route('employee.index')
            ->with('success', 'Data berhasil dihapus');
    }
}
