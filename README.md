# EMPLOYEE SOAL 1

## Requirement

- PHP 8.0
- Nodejs v16

## Front End

Bootstrap v5.1

## How to install?
Step 1 :

```php
composer install
```

Step 2 :

```php
npm install && npm run dev
```

Step 3 :

```php
copy env.example to .env
```

Step 4 :

```php
php artisan key:generate
```

Step 5 :

```php
php artisan migrate --seed
```

DONE!

## Account

| Role | Email | password | ability |
|---|---|---|---|
|admin|emanuel@gmail.com|P@ssw0rd!4dm1N!|Create, Read, Update , Delete|
|user|andre@gmail.com|password|Create, Read, Update|
